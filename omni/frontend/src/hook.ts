import { writable } from 'svelte/store';
import { goto } from '$app/navigation';
import { get } from 'svelte/store';



// Almacén para el token JWT
export const token = writable(null);
export const error = writable(null);

//eventos
export const username = writable('');
export const password = writable('');
export const role = writable ('');
export const profileLoaded = writable(false);





// Lógica para cerrar sesión y eliminar el token
export async function logout() {
    const iftoken = get(token);
    if (iftoken) {
        token.set(null); // Elimina el token del almacén}
        goto('/login')
        return false
    }
   return true
}




//login
export async function login() {
    const ispassword = get(password);
    const isusername = get(username);
    const formData = new URLSearchParams();
    formData.append('grant_type', '');
    formData.append('username', isusername);
    formData.append('password', ispassword);
    formData.append('scope', '');
    formData.append('client_id', '');
    formData.append('client_secret', '');

    try {
        const response = await fetch('http://0.0.0.0:8000/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',


            },
            body: formData,
        });

        if (response.ok) {
            const data = await response.json();
            token.set(data.access_token); // Almacena el token en el almacén
            error.set(null); // Borra cualquier error existente
            goto('/profile')
        } else {
            // @ts-ignore
            error.set('Credenciales incorrectas');
        }
        // @ts-ignore
    } catch ($error) {
        console.error('Error al realizar la solicitud:', $error);
        // @ts-ignore
        error.set('Error en el servidor');
    }
}

//GET PROFILE

export async function get_profile() {
    const tok = get(token);
    if (tok){
        try {
            const res = await fetch(`http://0.0.0.0:8000/profile?token=${tok}`);
            if (!res.ok) throw new Error("Failed to load profile");
         const data = await res.json();
         profileLoaded.set(true);
         return data;
        } catch (error) {
            console.log(error);
            
            return false;
            
        }
    }
    
   
         
        
    
}