# app/core/config.py
from fastapi import APIRouter
from pydantic import BaseSettings

router = APIRouter()


class Settings(BaseSettings):
    SECRET_KEY: str
    DATABASE_URL: str
    ALGORITHM: str
    class Config:
        env_file = ".env"

settings = Settings()
