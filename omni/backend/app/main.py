from fastapi import FastAPI
from api import auth, users
from core import config
from db import base, models
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()


# Configurar orígenes permitidos
origins = [
    "http://localhost:3000",
    "http://0.0.0.0",
    "http://0.0.0.0:3000",  # Ejemplo de origen para una aplicación Svelte en desarrollo
]

# Configurar middleware CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Incluye las rutas importadas
app.include_router(auth.router)
app.include_router(users.router)

app.include_router(config.router)


app.include_router(base.router)
app.include_router(models.router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

