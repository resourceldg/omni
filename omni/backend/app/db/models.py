# app/db/models.py
# app/db/models.py
from fastapi import APIRouter
from sqlalchemy.sql import func
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import Enum as SQLAlchemyEnum
from db.base import Base
from enum import Enum
router = APIRouter()

class RoleEnum(str, Enum):
    admin = 'admin'
    user = 'user'
    client = 'client'
    teacher = 'teacher'
    editor = 'editor'

class User(Base):
    __tablename__ = 'User'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, nullable=False)
    avatar_src = Column(String, nullable=True)
    email = Column(String, unique=True,default="email@example.com", nullable=False)
    first_name = Column(String, nullable=True)
    last_name = Column(String, nullable=True)
    password_hash = Column(String, nullable=False)
    role = Column(SQLAlchemyEnum(RoleEnum), default=RoleEnum.user, nullable=True)
    user_auth_token = Column(String, unique=True,nullable=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), server_default=func.now())