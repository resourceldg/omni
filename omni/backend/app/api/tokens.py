
from typing import Optional
from fastapi import APIRouter
from pydantic import BaseModel
router = APIRouter()

class Token(BaseModel):
    access_token: Optional[str] = None
    token_type: Optional[str] = None
