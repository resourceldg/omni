# app/api/users.py
from fastapi import APIRouter
from typing import Optional
from enum import Enum
from pydantic import BaseModel

router = APIRouter()
""" class UserResponse(BaseModel):
    id: Optional[int] = None
    username: Optional[str] = None
    email: Optional[str] = None
    fullname: Optional[str] = None
    disable: Optional[bool] = None
    hashed_password: Optional [str] = None


class GetUser(UserResponse):
    plain_password: str """



class RoleEnum(Enum):
    admin = 'admin'
    user = 'user'
    client = 'client'
    teacher = 'teacher'
    editor = 'editor'

class UserResponse(BaseModel):
    id: Optional[int] = None
    avatar_src: Optional[str] = None
    email: Optional[str] = None
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    full_name: Optional[str] = None
    password_hash: Optional[str] = None
    role: Optional[RoleEnum] = None
    user_auth_token: Optional[str] = None
    username: Optional[str] = None
    created_at: Optional[str] = None
    updated_at: Optional[str] = None




