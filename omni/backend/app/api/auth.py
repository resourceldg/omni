from fastapi import APIRouter, Depends, HTTPException
from datetime import datetime
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from passlib.context import CryptContext
from datetime import datetime, timedelta
from pydantic import BaseModel
from core.config import settings
from db.base import SessionLocal
from db.models import User
from api.users import UserResponse
from api.tokens import Token
from typing import Union

router = APIRouter()




# ++++++++++++AUTHENTICATION RULES++++++++++++++++++++++++++++++++++++

# Configuración de hashing de contraseñas
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_user_by_username(username):
    # Paso 1: Obtener una sesión de SQLAlchemy
    db = SessionLocal()

    try:
        # Paso 2: Realizar la consulta en la base de datos
        user = db.query(User).filter(User.username == username).first()

        # Paso 3: Verificar si se encontró un usuario
        if user:
            return user
        else:
            raise HTTPException(status_code=400, detail="User not found")
    finally:
        # Siempre cierra la sesión para evitar fugas de recursos
        db.close()


# Función para autenticar al usuario
def authenticate_user(username: str, password: str):
    user = get_user_by_username(username)
    if user is None or not verify_password(password, user.password_hash):
        return None
    return user
# Función para verificar contraseñas
def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

# Función para generar un token JWT
def create_access_token(data: dict, expires_minutes: int):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=expires_minutes)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt

# Función para obtener el usuario actual a partir del token JWT
def get_current_user(token):
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise HTTPException(status_code=400, detail="Could not validate credentials")
        
        user = get_user_by_username(username)
        
        if user is None:
            raise HTTPException(status_code=400, detail="User not found")
        
        return username
        
    except JWTError:
        raise HTTPException(status_code=400, detail="Could not validate credentials")




# +++++++++++++++++++++AUTENTICATION PATH'S ++++++++++++++++++++++++++++++++++++++



# Función para registrar un nuevo usuario en la base de datos

@router.post("/register", response_model=UserResponse)
async def register_user(user:UserResponse):
    # Paso 1: Obtener una sesión de SQLAlchemy
    db = SessionLocal()

    try:
        # Paso 2: Verificar si el usuario ya existe
        existing_user = db.query(User).filter(User.username == user.username).first()
        if existing_user:
            raise HTTPException(status_code=400, detail="User already registered")

        # Paso 3: Hashear la contraseña
        hashed_password = pwd_context.hash(user.password_hash)

        # Paso 4: Crear el nuevo usuario en la base de datos
        new_user = User(avatar_src=user.avatar_src, email=user.email,
                         first_name=user.first_name,last_name=user.last_name,
                          password_hash=hashed_password,
                           role=user.role.value, user_auth_token=user.user_auth_token,
                           username=user.username,created_at=datetime.utcnow(), updated_at=datetime.utcnow())
        db.add(new_user)
        db.commit()
        db.refresh(new_user)

        return UserResponse(username=new_user.username ,role=new_user.role)
    finally:
        # Siempre cierra la sesión para evitar fugas de recursos
        db.close()

# Ruta para iniciar sesión y obtener un token JWT

@router.post("/login", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if user is None:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    access_token = create_access_token(data={"sub": user.username}, expires_minutes=1)
    print(access_token)
    return {"access_token": access_token, "token_type": "bearer"}

# Definir la ruta protegida

@router.get("/profile", response_model=UserResponse)
async def read_user_profile(current_user: str = Depends(get_current_user)):
    # La función `get_current_user` se encarga de obtener el usuario actual
    # desde el token JWT. En este punto, `current_user` contendrá el nombre de usuario.
    # Aquí puedes realizar operaciones relacionadas con el perfil del usuario.
    
    # Por ejemplo, obtener el usuario desde la base de datos
    user = get_user_by_username(current_user)

    # Verificar si el usuario existe
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    # Devolver el perfil del usuario
    return UserResponse(
        username=user.username,
        email=user.email,
        role=user.role
    )